﻿namespace MythFightLauncher
{
    partial class MythFight
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MythFight));
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.ServVers = new System.Windows.Forms.Label();
            this.your = new System.Windows.Forms.Label();
            this.server = new System.Windows.Forms.Label();
            this.Exit = new System.Windows.Forms.Button();
            this.labelPerc = new System.Windows.Forms.Label();
            this.labelDownloaded = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.Option = new System.Windows.Forms.LinkLabel();
            this.Startbtn = new System.Windows.Forms.PictureBox();
            this.Homepage = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Startbtn)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.ForeColor = System.Drawing.SystemColors.Info;
            this.progressBar1.Location = new System.Drawing.Point(12, 451);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(485, 21);
            this.progressBar1.TabIndex = 0;
            // 
            // ServVers
            // 
            this.ServVers.AutoSize = true;
            this.ServVers.BackColor = System.Drawing.Color.Transparent;
            this.ServVers.ForeColor = System.Drawing.Color.White;
            this.ServVers.Location = new System.Drawing.Point(10, 432);
            this.ServVers.Name = "ServVers";
            this.ServVers.Size = new System.Drawing.Size(97, 13);
            this.ServVers.TabIndex = 0;
            this.ServVers.Text = "Aktuellste Version :";
            // 
            // your
            // 
            this.your.AutoSize = true;
            this.your.BackColor = System.Drawing.Color.Transparent;
            this.your.ForeColor = System.Drawing.Color.White;
            this.your.Location = new System.Drawing.Point(564, 45);
            this.your.Name = "your";
            this.your.Size = new System.Drawing.Size(10, 13);
            this.your.TabIndex = 4;
            this.your.Text = "-";
            this.your.Click += new System.EventHandler(this.your_Click);
            // 
            // server
            // 
            this.server.AutoSize = true;
            this.server.BackColor = System.Drawing.Color.Transparent;
            this.server.ForeColor = System.Drawing.Color.White;
            this.server.Location = new System.Drawing.Point(107, 432);
            this.server.Name = "server";
            this.server.Size = new System.Drawing.Size(10, 13);
            this.server.TabIndex = 5;
            this.server.Text = "-";
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.Transparent;
            this.Exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Exit.BackgroundImage")));
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Location = new System.Drawing.Point(599, 4);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(29, 29);
            this.Exit.TabIndex = 6;
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // labelPerc
            // 
            this.labelPerc.AutoSize = true;
            this.labelPerc.BackColor = System.Drawing.Color.Transparent;
            this.labelPerc.ForeColor = System.Drawing.Color.White;
            this.labelPerc.Location = new System.Drawing.Point(327, 370);
            this.labelPerc.Name = "labelPerc";
            this.labelPerc.Size = new System.Drawing.Size(10, 13);
            this.labelPerc.TabIndex = 9;
            this.labelPerc.Text = "-";
            // 
            // labelDownloaded
            // 
            this.labelDownloaded.AutoSize = true;
            this.labelDownloaded.Location = new System.Drawing.Point(466, 432);
            this.labelDownloaded.Name = "labelDownloaded";
            this.labelDownloaded.Size = new System.Drawing.Size(31, 13);
            this.labelDownloaded.TabIndex = 10;
            this.labelDownloaded.Text = "MB´s";
            this.labelDownloaded.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(425, 432);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(0, 65);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(640, 360);
            this.webBrowser1.TabIndex = 13;
            this.webBrowser1.Url = new System.Uri("http://ceratic.bplaced.de/mf/client/html", System.UriKind.Absolute);
            // 
            // Option
            // 
            this.Option.ActiveLinkColor = System.Drawing.Color.White;
            this.Option.AutoSize = true;
            this.Option.BackColor = System.Drawing.Color.Transparent;
            this.Option.DisabledLinkColor = System.Drawing.Color.White;
            this.Option.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option.ForeColor = System.Drawing.Color.White;
            this.Option.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
            this.Option.LinkColor = System.Drawing.Color.White;
            this.Option.Location = new System.Drawing.Point(86, 46);
            this.Option.Name = "Option";
            this.Option.Size = new System.Drawing.Size(72, 17);
            this.Option.TabIndex = 16;
            this.Option.TabStop = true;
            this.Option.Text = "Einstellungen";
            this.Option.UseCompatibleTextRendering = true;
            this.Option.VisitedLinkColor = System.Drawing.Color.White;
            this.Option.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Startbtn
            // 
            this.Startbtn.BackColor = System.Drawing.Color.Transparent;
            this.Startbtn.BackgroundImage = global::launcher.Properties.Resources.start_dis;
            this.Startbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Startbtn.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Startbtn.Location = new System.Drawing.Point(503, 427);
            this.Startbtn.Name = "Startbtn";
            this.Startbtn.Size = new System.Drawing.Size(130, 50);
            this.Startbtn.TabIndex = 17;
            this.Startbtn.TabStop = false;
            this.Startbtn.Click += new System.EventHandler(this.Startbtn_Click_1);
            this.Startbtn.MouseLeave += new System.EventHandler(this.Startbtn_MouseLeave);
            this.Startbtn.MouseHover += new System.EventHandler(this.Startbtn_MouseHover);
            // 
            // Homepage
            // 
            this.Homepage.ActiveLinkColor = System.Drawing.Color.White;
            this.Homepage.AutoSize = true;
            this.Homepage.BackColor = System.Drawing.Color.Transparent;
            this.Homepage.DisabledLinkColor = System.Drawing.Color.White;
            this.Homepage.LinkColor = System.Drawing.Color.White;
            this.Homepage.Location = new System.Drawing.Point(9, 45);
            this.Homepage.Name = "Homepage";
            this.Homepage.Size = new System.Drawing.Size(59, 13);
            this.Homepage.TabIndex = 19;
            this.Homepage.TabStop = true;
            this.Homepage.Text = "Homepage";
            this.Homepage.VisitedLinkColor = System.Drawing.Color.White;
            this.Homepage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Homepage_LinkClicked_1);
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.DisabledLinkColor = System.Drawing.Color.White;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.ForeColor = System.Drawing.Color.White;
            this.linkLabel1.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(468, 45);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(29, 17);
            this.linkLabel1.TabIndex = 20;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Über";
            this.linkLabel1.UseCompatibleTextRendering = true;
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.White;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // MythFight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::launcher.Properties.Resources.bg;
            this.ClientSize = new System.Drawing.Size(640, 480);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.Homepage);
            this.Controls.Add(this.Startbtn);
            this.Controls.Add(this.Option);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelDownloaded);
            this.Controls.Add(this.labelPerc);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.server);
            this.Controls.Add(this.your);
            this.Controls.Add(this.ServVers);
            this.Controls.Add(this.progressBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MythFight";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TransparencyKey = System.Drawing.SystemColors.ActiveCaption;
            this.Load += new System.EventHandler(this.MythFightLauncher_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Startbtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label ServVers;
        public System.Windows.Forms.Label your;
        public System.Windows.Forms.Label server;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Label labelPerc;
        private System.Windows.Forms.Label labelDownloaded;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.LinkLabel Option;
        private System.Windows.Forms.PictureBox Startbtn;
        private System.Windows.Forms.LinkLabel Homepage;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

