﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;

namespace MythFightLauncher
{
    public partial class MythFight : Form
    {
        public delegate void DoWorkDelegate();
        WebClient Verbindung = new WebClient();


        private void Startbtn_MouseHover(object sender, EventArgs e)
        {
            
            Startbtn.Image = launcher.Properties.Resources.Btn2;
        }
        private void Startbtn_MouseLeave(object sender, EventArgs e)
        {
            
            Startbtn.Image = launcher.Properties.Resources.Btn;
        }

        public MythFight()
        {
            InitializeComponent();
        }

        public static string GetMD5Hash(string TextToHash)
        {
            if (string.IsNullOrEmpty(TextToHash) | TextToHash.Length == 0)
            {
                return string.Empty;
            }

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] toHash = Encoding.Default.GetBytes(TextToHash);
            byte[] result = md5.ComputeHash(toHash);

            return System.BitConverter.ToString(result);
        }

        public void MythFightLauncher_Load(object sender, EventArgs e)
        {
            this.Testa();
            
            
        }

        
        public void Testa()
        {
                int u = 1;
                //int c = 1;
                int c = Convert.ToInt32(server.Text) + u;
                try
                {
                    //Es wird ein neues Update gestartet
                    if (Convert.ToInt32(your.Text) < Convert.ToInt32(server.Text))
                    {
                            Verbindung.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

                            Verbindung.DownloadFileCompleted += delegate
                            {
                                try
                                {
                                    //Process oProcess = null;
                                    var oProcess = Process.Start("UnRAR.exe", "x -y -ac " + your.Text + ".rar");
                                    //extract.extract()
                                    //Patch löschen
                                    oProcess.WaitForExit();
                                    File.Delete(your.Text + ".rar");

                                    int ergebnis = Convert.ToInt32(your.Text) + u;
                                    your.Text = ergebnis.ToString();
                                    string Client = @"Client.dat";
                                    File.WriteAllText(Client, your.Text);
                                    this.Testa();
                                }
                                catch
                                {

                                }
                            };
                            // Start downloading the file

                            Verbindung.DownloadFileAsync(new Uri("http://ceratic.bplaced.de/mf/client/" + your.Text + ".rar"), your.Text + ".rar");

                        

                    }
                    else if (your.Text == server.Text)
                    {
                        string tst = label1.Text;

                        if (Convert.ToUInt32(your.Text) > Convert.ToUInt32(tst))
                        {
                        Verbindung.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

                        Verbindung.DownloadFileCompleted += delegate
                        {

                            try
                            {
                                //Process oProcess = null;
                                var oProcess = Process.Start("UnRAR.exe", "x -y -ac " + your.Text + ".rar");
                                //extract.extract()
                                //Patch löschen
                                oProcess.WaitForExit();
                                File.Delete(your.Text + ".rar");

                                int ergebnis = Convert.ToInt32(your.Text) + u;
                                your.Text = ergebnis.ToString();

                                string Client = @"Client.dat";
                                File.WriteAllText(Client, your.Text);
                                this.Testa();
                            }
                            catch
                            {

                            }
                        };
                        // Start downloading the file
                        Verbindung.DownloadFileAsync(new Uri("http://ceratic.bplaced.de/mf/client/" + server.Text + ".rar"), server.Text + ".rar");
                        }

                        else
                        {
                            your.Text = server.Text;

                            string Client = @"Client.dat";
                            File.WriteAllText(Client, your.Text);

                            Startbtn.Enabled = true;
                            Startbtn.Image = launcher.Properties.Resources.Btn;
                    }
                    }
                    else if (your.Text == c.ToString())
                    {
                        your.Text = server.Text;

                        string Client = @"Client.dat";
                        File.WriteAllText(Client, your.Text);

                        Startbtn.Enabled = true;
                        Startbtn.Image = launcher.Properties.Resources.Btn;
                }
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
                    
        }

        // The event that will fire whenever the progress of the WebClient is changed
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // Update the progressbar percentage only when the value is not the same.
            progressBar1.Value = e.ProgressPercentage;
            
            // Show the percentage on our label.
            labelPerc.Text = e.ProgressPercentage.ToString() + "%";

            // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
            labelDownloaded.Text = string.Format("{0} MB's / {1} MB's",
                (e.BytesReceived / 1024d / 1024d).ToString("0.00"),
                (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
        }

        private void Startbtn_Click_1(object sender, EventArgs e)
        {
            
                Process.Start("MythFight.exe");
                Application.Exit();
           
        }

        private void CheckEnter(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                this.Startbtn_Click_1(this, EventArgs.Empty);
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

      
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Option option = new Option();

            option.Show();
        }



        private void Homepage_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://larsbehrends.de/mf/");
        }

        private void your_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AboutBox1 AboutBox = new AboutBox1();
            AboutBox.Show();
        }
    }
}
